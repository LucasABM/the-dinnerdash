# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

MealCategory.create([{name: 'Hamburguers'}, {name: 'Massas'}])
Meal.create([{name: 'X-Burguer', description: 'Só burgue queije pão mesmo', price: 4.50, avaible: 30, meal_category_id: 1},
    {name: 'Spoletin', description: 'Escolha os ingredientes vc mesmo TM', price: 18.50, avaible: 15, meal_category_id: 2}])

#seeds para teste do ordermeals
Situation.create(description: 'Ó, meu patrão, deve estar tudo bem')
User.create(name: "Lucas", email: "emailzinho@hotmail.com", password: "senhadoida123", admin: "1")
Order.create(price: 150, user_id: 1, situation_id: 1)
###########