Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  resources :users, param: :_name
  post '/auth/login', to: 'authentication#login'
  get '/*', to: 'application#not_found'

  get "/categories/index", to: "meal_categories#index"
  get "/categories/show/:id", to: "meal_categories#show"
  post "/categories/create", to: "meal_categories#create"
  put "/categories/update/:id", to: "meal_categories#update"
  patch "/categories/update/:id", to: "meal_categories#update"
  delete "/categories/delete/:id", to: "meal_categories#delete"

  get "/meals/index", to: "meals#index"
  get "/meals/show/:id", to: "meals#show"
  post "/meals/create", to: "meals#create"
  put "/meals/update/:id", to: "meals#update"
  patch "/meals/update/:id", to: "meals#update"
  delete "/meals/delete/:id", to: "meals#delete"

  get "/order_meals/index", to: "order_meals#index"
  get "/order_meals/show/:id", to: "order_meals#show"
  post "/order_meals/create", to: "order_meals#create"
  put "/order_meals/update/:id", to: "order_meals#update"
  patch "/order_meals/update/:id", to: "order_meals#update"
  delete "/order_meals/delete/:id", to: "order_meals#delete"

  get "/orders/index", to: "orders#index"
  get "/orders/show/:id", to: "orders#show"
  post "/orders/create", to: "orders#create"
  patch "/orders/update/:id", to: "orders#update"
  delete "/orders/delete/:id", to: "orders#destroy"

  get "/situations/index", to: "situations#index"
  get "/situations/show/:id", to: "situations#show"
  post "/situations/create", to: "situations#create"
  put "/situations/update/:id", to: "situations#update"
  patch "/situations/update/:id", to: "situations#update"
  delete "/situations/delete/:id", to: "situations#destroy"
end
