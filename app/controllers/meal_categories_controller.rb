class MealCategoriesController < ApplicationController

    before_action :authorize_request
    before_action :find_cat, only: [:show, :update, :delete]

    #GET /categories/index
    def index
        @q = MealCategory.ransack(params[:q])
        render json: @q.result(distinct: true)
    end

    #GET /categories/show/:id
    def show
        render json: @cat
    end

    #POST /categories/create
    def create
        @cat = MealCategory.new(parameters)
        if @cat.save
            render json: @cat, status: 200

        else
            render json: @cat.errors
        end
    end

    #PATCH, PUT /categories/update/:id
    def update
        if @cat.update(parameters)
            render json: @cat
        else
            render json: @cat.errors
        end
    end

    #DELETE /categories/delete/:id
    def delete
        @cat.destroy
    end

    private

    def parameters
        params.require(:meal_category).permit(:name)
    end

    def find_cat
        @cat = MealCategory.find(params[:id])
    end

end
