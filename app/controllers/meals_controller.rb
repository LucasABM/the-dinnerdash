class MealsController < ApplicationController

    before_action :authorize_request
    before_action :find_meal, only: [:show, :update, :delete]

    #GET /meals/index
    def index
        @q = Meal.ransack(params[:q])
        render json: @q.result(distinct: true)
    end

    #GET /meals/show/:id
    def show
        render json: @meal
    end

    #POST /meals/create
    def create
        @meal = Meal.new(parameters)
        if @meal.save
            render json: @meal, status: 200

        else
            render json: @meal.errors
        end
    end

    #PATCH, PUT /meals/update/:id
    def update
        if @meal.update(parameters)
            render json: @meal
        else
            render json: @meal.errors
        end
    end

    #DELETE /meals/delete/:id
    def delete
        @meal.destroy
    end

    private

    def parameters
        params.permit(:name, :description, :price, :avaible, :meal_category_id, :meal_img)
    end

    def find_meal
        @meal = Meal.find(params[:id])
    end
end
