class OrderMealsController < ApplicationController

    before_action :authorize_request
    before_action :find_order_meal, only: [:show, :update, :delete]

    #GET /order_meals/index
    def index
        @q = OrderMeal.ransack(params[:q])
        render json: @q.result(distinct: true)
    end

    #GET /order_meals/show/:id
    def show
        render json: @order_meals
    end

    #POST /order_meals/create
    def create
        @order_meals = OrderMeal.new(parameters)
        if @order_meals.save
            render json: @order_meals, status: 200

        else
            render json: @order_meals.errors
        end
    end

    #PATCH, PUT /order_meals/update/:id
    def update
        if @order_meals.update(parameters)
            render json: @order_meals
        else
            render json: @order_meals.errors
        end
    end

    #DELETE /order_meals/delete/:id
    def delete
        @order_meals.destroy
    end

    private

    def parameters
        params.require(:order_meal).permit(:quantity, :meal_id, :order_id)
    end

    def find_order_meal
        @order_meals = OrderMeal.find(params[:id])
    end
end
