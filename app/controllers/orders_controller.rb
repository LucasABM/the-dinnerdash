class OrdersController < ApplicationController

    before_action :authorize_request
    before_action :set_order, only: [:show, :update, :destroy]

    #GET /orders
    #Pega todos as orders do BD e reinderiza no Json (obter Dados)
    def index
        @q = Order.ransack(params[:q])
        render json: @q.result(distinct: true)
    end
    #GET /orders/:id
    def show 
        #Poderia ser qualquer parametro que temos na nossa model
        render json: @order, status: 201
    end

    #POST /orders
    def create
        @order = Order.new(order_params)
        if @order.save
            render json: @order, status: 200
        else
            render json: @order.errors, status: :unprocessable_entity
        end
    end
    #PUT ou PATCH /orders
    def update
       
        #Quando dermos update, poderemos editar os parametros listados abaixo
        if @order.update(order_params)
            render json: @order, status: 200
        else
            render json: @order, status: :unprocessable_entity
        end
    end

    #DELETE "/orders/:id"
    def destroy
        
        #usamos o destroy, pq ele roda os callbacks antes de executar
        @order.destroy(order_params)
    end
    # Criando método privado
    private 
        def set_order
            @order = Order.find(params[:id])
        end
        def order_params
            params.require(:order).permit(:price, :user_id, :situation_id)
        end
end
