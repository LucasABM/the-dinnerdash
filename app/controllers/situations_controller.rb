class SituationsController < ApplicationController

    before_action :authorize_request
    before_action :set_situation, only: [:show, :update, :destroy]

    #GET /situations
    #Pega todos as situations do BD e reinderiza no Json (obter Dados)
    def index
        @situations = Situation.all
        render json:@situations, status: 200
    end
    #GET /situations/:id
    def show 
        #Poderia ser qualquer parametro que temos na nossa model
        render json: @situation, status: 201
    end

    #POST /situations
    def create
        @situation = Situation.new(situation_params)
        if @situation.save
            render json: @situation, status: 200
        else
            render json: @situation.errors, status: :unprocessable_entity
        end
    end
    #PUT ou PATCH /situations
    def update
       
        #Quando dermos update, poderemos editar os parametros listados abaixo
        if @situation.update(situation_params)
            render json: @situation, status: 200
        else
            render json: @situation, status: :unprocessable_entity
        end
    end

    #DELETE "/situations/:id"
    def destroy
        
        #usamos o destroy, pq ele roda os callbacks antes de executar
        @situation.destroy(situation_params)
    end
    # Criando método privado
    private 
        def set_situation
            @situation = Situation.find(params[:id])
        end
        def situation_params
            params.require(:situation).permit(:description)
        end
end
