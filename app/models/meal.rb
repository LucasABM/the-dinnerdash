class Meal < ApplicationRecord
    validates :name, presence: true
    validates :name, length: {maximum: 45} 

    validates :price, presence: true
    validates :price, numericality: true

    validates :avaible, presence: true
    validates :avaible, numericality: { only_integer: true }

    validates :description, presence: true
    validates :description, length: {maximum: 500}

    has_one_attached :meal_img
    belongs_to :meal_category
end
