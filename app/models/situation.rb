class Situation < ApplicationRecord
    validates :description, presence: true
    has_many:orders
end
