class Order < ApplicationRecord

    has_many:order_meals
    has_one:user
    has_one:situation

    after_create :set_price

    def set_price
        order = Order.find(self.id)
        order.price = 0
        order.save
    end
end
