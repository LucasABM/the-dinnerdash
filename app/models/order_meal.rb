class ValidateQuantity < ActiveModel::Validator
    def validate(record)
        atual = Meal.find(record.meal_id).avaible
        
        if (record.id)
            atual += OrderMeal.find(record.id).quantity
        end

        if (record.quantity > atual)
            record.errors.add :base, "Quantidade não disponível"
        end
    end
end

class OrderMeal < ApplicationRecord
    validates :quantity, presence: true
    validates :quantity, numericality: { only_integer: true }
    validates_with ValidateQuantity

    belongs_to :meal
    belongs_to :order

    after_create :update_avaible
    before_update :revert_avaible
    after_update :update_avaible
    before_destroy :revert_avaible

    before_create :update_price
    before_update :update_price

    def revert_avaible
        puts Meal.find(self.meal_id).avaible
        @temp = Meal.find(self.meal_id).avaible
        Meal.update(self.meal_id, :avaible => @temp + OrderMeal.find(self.id).quantity)
    end

    def update_avaible
        puts Meal.find(self.meal_id).avaible    
        @temp = Meal.find(self.meal_id).avaible
        Meal.update(self.meal_id, :avaible => @temp - self.quantity)
    end

    def update_price
        puts Meal.find(self.meal_id).price
        puts Order.find(self.order_id).price
        @temp = self.quantity
        @temp2 = Meal.find(self.meal_id).price

        Order.update(self.order_id, :price => @temp * @temp2)
    end

end
